/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.sm.da;

import exam.sm.entity.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author viett
 */
public class StudentManager {

    public List<Student> getListStudent() {
        try {
            Connection connection = DBConnection.getConnection();
            List<Student> list = new LinkedList<Student>();
            PreparedStatement statement = connection.prepareStatement("select * form StudentManager");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int rollNumber = rs.getInt("rollNumber");
                String name = rs.getString("name");
                String className = rs.getString("className");
                String email = rs.getString("email");
                list.add(new Student(rollNumber, name, className, email));
            }
            return list;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StudentManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(StudentManager.class.getName()).log(Level.SEVERE, null, ex);

        }
        return new LinkedList<Student>();
    }

    public boolean registerStudent(int rollNumber, String name, String nameClass, String email) {
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement("insert into StudentManager (rollNumber, name, nameClass, email) values (?,?,?,?)");
            statement.setInt(1, rollNumber);
            statement.setString(2, name);
            statement.setString(3, nameClass);
            statement.setString(4, email);
            statement.executeQuery();
            return true;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StudentManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(StudentManager.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }
}
