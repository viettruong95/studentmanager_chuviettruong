<%-- 
    Document   : index
    Created on : Jan 4, 2019, 10:14:45 AM
    Author     : viett
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
            <s:iterator value="students" var="student">
                <tr><s:property value="rollNumber" /></tr>
                <tr><s:property value="name" /></tr>
                <tr><s:property value="className" /></tr>
                <tr><s:property value="email" /></tr>
            </s:iterator>
        </table>
        <a href="register.jsp">Register</a>
    </body>
</html>
