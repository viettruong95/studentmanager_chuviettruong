<%-- 
    Document   : register
    Created on : Jan 4, 2019, 10:16:36 AM
    Author     : viett
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="registerStudent">
            Roll Number: <input type="text" name="rollNumber" id="rollNumber" required>
            Name: <input type="text" name="name" id="name" required>
            Email: <input type="email" name="email" id="email" required>
            Class Name: <input type="text" name="className" id="className" required>
            <input type="submit" value="Register">
            <button href="index.jsp">List Student</button>
        </form>
    </body>
</html>
